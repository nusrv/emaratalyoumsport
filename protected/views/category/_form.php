<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',

)); ?>

	<?php echo $form->errorSummary($model); ?>
	<a href="#"  onclick="guiders.show('firstSection');return false"    ><i class="fa fa-question-circle"></i></a>

	<?php echo $form->textFieldGroup($model,'title'); ?>



 	<div class="col-sm-12">
<div class="col-sm-5">
	<div class="col-sm-2  col-sm-offset-7"  style="margin-top:10px;">
	<a href="#"  onclick="guiders.show('secondSection');return false"    ><i class="fa fa-question-circle"></i></a>
</div>
	<div class="col-sm-3  col-sm-pull-1">
	<?php echo $form->checkboxGroup($model,'active'); ?>
		</div>
</div>
		</div>
	<div class="form-actions  pull-right" style="margin-bottom: 20px">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => $model->isNewRecord ? 'Create' : 'Save'
			)
		); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstSection',
		'title'        => 'Title',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Section  title',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Category_title',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'secondSection',
		'title'        => 'Status',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The status of this category if its checked the program will get posts from this section',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Category_active',
		'position'      => 11,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $form CActiveForm */

?>

<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
	<div class="pad margin no-print">
		<div class="callout callout-success" style="margin-bottom: 0!important;">
			<h4><i class="fa fa-info"></i> Note:</h4>
			<?php echo $message?>
		</div>
	</div>
<?PHP } ?>
<style>
	.form-inline .form-group {
		vertical-align: top;
		width: 100%;
		margin-bottom: 15px;
	}
	.form-inline .form-control {
		display: inline-block;
		width: 100%;
		vertical-align: middle;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header">
					<div class="box-title">
						<h3>Generate PDF file</h3>
					</div>
				</div>
				<div class="box-body">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'post-queue-form',
            'action' => Yii::app()->createUrl('searchForm/downloadpdffile'),  //<- your form action here
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype' => 'multipart/form-data', 'class'=>'form-inline'),
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?PHP if(!empty($form->errorSummary($model))){ ?><div class="callout callout-danger"><?php echo $form->errorSummary($model); ?></div><?PHP } ?>

					<div class="form-group">
		<div class="col-md-3"><?php echo $form->labelEx($model,'from'); ?></div>
		<div class="col-md-9">
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$model,
				'attribute'=>'from',
				'name'=>'from',
				//'value'=>isset($_POST["from"]) ? $_POST["from"] : '',
				// additional javascript options for the date picker plugin
				'options'=>array(
					'changeMonth'=>true,
					'changeYear'=>true,
					'dateFormat' => 'yy-mm-dd',
					//'minDate'=> 1
				),
				'htmlOptions'=>array(
					'class'=>'form-control ',
					'readonly'=>'readonly',
				),
			));
			?>
			<?php echo $form->error($model,'from'); ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-3"><?php echo $form->labelEx($model,'to'); ?></div>
		<div class="col-md-9">
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$model,
				'attribute'=>'to',
				'name'=>'to',
				//'value'=>isset($_POST["from"]) ? $_POST["from"] : '',
				// additional javascript options for the date picker plugin
				'options'=>array(
					'changeMonth'=>true,
					'changeYear'=>true,
					'dateFormat' => 'yy-mm-dd',
					//'minDate'=> 1
				),
				'htmlOptions'=>array(
					'class'=>'form-control ',
					'readonly'=>'readonly',
				),
			));
			?>
			<?php echo $form->error($model,'to'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('Download',array('class'=>'btn btn-block btn-flat btn-success')); ?>
	</div>

	<?php $this->endWidget(); ?>

				</div>

			</div>
		</div>
	</div>
</section>

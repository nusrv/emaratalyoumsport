<!DOCTYPE html>
<html lang="en">
<head>

    <!-- start: Meta -->
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- end: Meta -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.datetimepicker.css" rel="stylesheet">

    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.7.1.min.js"></script>
    <!-- start: CSS -->
    <link id="bootstrap-style" href="<?php echo Yii::app()->request->baseUrl; ?>/css-metro/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css-metro/bootstrap-responsive.min.css" rel="stylesheet">
    <link id="base-style" href="<?php echo Yii::app()->request->baseUrl; ?>/css-metro/style.css" rel="stylesheet">
    <link id="base-style-responsive" href="<?php echo Yii::app()->request->baseUrl; ?>/css-metro/style-responsive.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
    <!-- end: CSS -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link id="ie-style" href="<?php echo Yii::app()->request->baseUrl; ?>/css-metro/ie.css" rel="stylesheet">
    <![endif]-->

    <!--[if IE 9]>
    <link id="ie9style" href="<?php echo Yii::app()->request->baseUrl; ?>/css-metro/ie9.css" rel="stylesheet">
    <![endif]-->

    <!-- start: Favicon -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/favicon.ico">
    <!-- end: Favicon -->

<style>
    .errorMessage{
        color:red;
    }
    .errorSummary ul li{
        color:red;
    }

        .no-image{
            background-color: orange;
        }

    counter-style{
        margin-left: 50px;
        color: blue;
    }
    </style>
</head>

<body>
<!-- start: Header -->
<div class="navbar">
<div class="navbar-inner">
<div class="container-fluid">
<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<a class="brand" href="<?php echo Yii::app()->createUrl('/site/index') ?>"><span><?php echo CHtml::encode(Yii::app()->name); ?></span></a>

<!-- start: Header Menu -->
<div class="nav-no-collapse header-nav">
<ul class="nav pull-right">

<!--
<li>
    <a class="btn" href="#">
        <i class="halflings-icon white wrench"></i>
    </a>
</li>
-->
<!-- start: User Dropdown -->
<li class="dropdown">
    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="halflings-icon white user"></i> <Admin></Admin>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="dropdown-menu-title">
            <span>Account Settings</span>
        </li>
        <!--<li><a href="#"><i class="halflings-icon user"></i> Profile</a></li>-->
        <li><a href="<?php echo Yii::app()->createUrl('/site/logout') ?>"><i class="halflings-icon off"></i> Logout</a></li>
    </ul>
</li>
<!-- end: User Dropdown -->
</ul>
</div>
<!-- end: Header Menu -->

</div>
</div>
</div>
<!-- start: Header -->

<div class="container-fluid-full">
    <div class="row-fluid">

        <!-- start: Main Menu -->
        <div id="sidebar-left" class="span2">
            <div class="nav-collapse sidebar-nav">
                <ul class="nav nav-tabs nav-stacked main-menu">
                    <li>
                        <a class="dropmenu" href="#" title="FB Cover Photo"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">Library </span><span class="label label-important"> 5 </span></a>
                        <ul>
                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/create') ?>" title="Create Manual Post"><i class="icon-tasks"></i><span class="hidden-tablet"> Create Manual Post </span></a></li>

                            <li><a href="<?php echo Yii::app()->createUrl('/programs/admin') ?>" title="View Programs"><i class="icon-eye-open"></i><span class="hidden-tablet">Sections</span></a></li>

                            <li><a href="<?php echo Yii::app()->createUrl('/fillerData/admin') ?>" title="View Fillers"><i class="icon-eye-open"></i><span class="hidden-tablet">Fillers</span></a></li>

                            <li><a href="<?php echo Yii::app()->createUrl('/fbCoverPhoto/admin') ?>" title="View Covers"><i class="icon-eye-open"></i><span class="hidden-tablet">Cover Photo</span></a></li>

                            <li><a href="<?php echo Yii::app()->createUrl('/postTemplate/admin') ?>" title="View Covers"><i class="icon-eye-open"></i><span class="hidden-tablet">Post Templates</span></a></li>

                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu" href="#" title="FB Cover Photo"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">Generate Calendar </span><span class="label label-important"> 2 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/datesGenerated/downloadpdf') ?>" title="Download Pdf"><i class="icon-tasks"></i><span class="hidden-tablet"> Download Pdf </span></a></li>


                        </ul>
                    </li>

                    <li>
                        <a class="dropmenu" href="#" title="Post Queue"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet"> Post Queue</span> <span class="label label-important"> 3 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/facebook') ?>" title="Facebook"><i class="icon-eye-open"></i><span class="hidden-tablet"> Facebook</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/twitter') ?>" title="Twitter"><i class="icon-eye-open"></i><span class="hidden-tablet"> Twitter</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/instagram') ?>" title="Instagram"><i class="icon-eye-open"></i><span class="hidden-tablet"> Instagram</span></a></li>


                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu" href="#" title="UnSchedule Posts"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">UnSchedule Posts</span> <span class="label label-important"> 3 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/UnScheduleFacebook') ?>" title="Facebook"><i class="icon-eye-open"></i><span class="hidden-tablet"> Facebook</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/UnScheduleTwitter') ?>" title="Twitter"><i class="icon-eye-open"></i><span class="hidden-tablet"> Twitter</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/postQueue/UnScheduleInstagram') ?>" title="Instagram"><i class="icon-eye-open"></i><span class="hidden-tablet"> Instagram</span></a></li>


                        </ul>
                    </li>

                  <!--  <li>
                        <a class="dropmenu" href="#" title="programs"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">Sections</span> <span class="label label-important"> 2 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/programs/admin') ?>" title="View Programs"><i class="icon-eye-open"></i><span class="hidden-tablet">View Programs</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/programs/create') ?>" title="Add Program"><i class="icon-tasks"></i><span class="hidden-tablet">Add Program</span></a></li>


                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu" href="#" title="Filler"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">Filler</span> <span class="label label-important"> 2 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/fillerData/admin') ?>" title="View Fillers"><i class="icon-tasks"></i><span class="hidden-tablet">View Fillers</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/fillerData/create') ?>" title="Add New"><i class="icon-tasks"></i><span class="hidden-tablet">Add New</span></a></li>


                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu" href="#" title="FB Cover Photo"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">FB Cover Photo</span> <span class="label label-important"> 2 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/fbCoverPhoto/admin') ?>" title="View Covers"><i class="icon-tasks"></i><span class="hidden-tablet">View Covers</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/fbCoverPhoto/create') ?>" title="Add New"><i class="icon-tasks"></i><span class="hidden-tablet">Add New</span></a></li>


                        </ul>
                    </li>

                    <li>
                        <a class="dropmenu" href="#" title="FB Cover Photo"><i class="icon-folder-close-alt"></i><span
                                class="hidden-tablet">Post Template</span> <span class="label label-important"> 2 </span></a>
                        <ul>

                            <li><a href="<?php echo Yii::app()->createUrl('/postTemplate/admin') ?>" title="View Covers"><i class="icon-tasks"></i><span class="hidden-tablet">Manage Templates</span></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/postTemplate/create') ?>" title="Add New"><i class="icon-tasks"></i><span class="hidden-tablet">Add New</span></a></li>


                        </ul>
                    </li> -->
                </ul>
            </div>
        </div>
        <!-- end: Main Menu -->

        <noscript>
            <div class="alert alert-block span10">
                <h4 class="alert-heading">Warning!</h4>
                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
            </div>
        </noscript>

        <!-- start: Content -->
        <div id="content" class="span10">



        <?php echo $content; ?>


        </div>
        <!--/.fluid-container-->

        <!-- end: Content -->
    </div>
    <!--/#content.span10-->
</div>
<!--/fluid-row-->

<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Settings</h3>
    </div>
    <div class="modal-body">
        <p>Here settings can be configured...</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
    </div>
</div>

<div class="clearfix"></div>

<footer>



    <p>
        <span style="text-align:left;float:left">&copy; <?php echo date('Y');?> <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">NUSRV</a></span>

    </p>

</footer>

<script>

    function change_counter_twitter(with_image) {
        var num=140;
        if(with_image){
            num -=24;
        }
        var input = num-$("#PostQueue_post").val().length;
        $("#counter").text(input);
    }

    function activate_post(id){

        var result = confirm('Are you sure you want to make it active ?');
        if (result == true) {
            $.ajax({
                method: "POST",
                url: "<?php echo CController::createUrl('postQueue/ActivatePost')?>",
                data: { reg_id: id}
            })
                .done(function( msg ) {
                    $('#row-'+id).remove();
                });
        }
    }
</script>
<!-- start: JavaScript-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-bbq.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-migrate-1.0.0.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.0.custom.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ui.touch-punch.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.cookie.js"></script>

<script src='<?php echo Yii::app()->request->baseUrl; ?>/js/fullcalendar.min.js'></script>

<script src='<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables.min.js'></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/excanvas.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flot.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flot.pie.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flot.stack.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flot.resize.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.chosen.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.uniform.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.cleditor.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.noty.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.elfinder.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.raty.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.iphone.toggle.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.uploadify-3.1.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.gritter.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.imagesloaded.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.masonry.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.knob.modified.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.sparkline.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.colorPicker.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/counter.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/retina.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.datetimepicker.full.js"></script>

<!-- end: JavaScript-->

</body>
</html>


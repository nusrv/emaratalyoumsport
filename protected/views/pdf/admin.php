<?php
/* @var $this PdfController */
/* @var $model Pdf */

$this->pageTitle = "PDF | Admin";

$this->breadcrumbs=array(
	'PDF File'=>array('admin'),
	'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#pdf-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
	<style>
		.checkbox {
			display: block;
			min-height: 0px;
			margin-top: 4px;
			margin-bottom: 0px;
			padding-left: 17px;
		}
		.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
			margin-top: 0;
		}
		.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
			float: left;
			margin-left: -16px;
		}
	</style>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Create',
										'buttonType' =>'link',
										'url' => array('pdf/index')
									),
								),

							)
						);

						?></div>
					<div class="col-sm-3" style="text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>





					</div>
				</div>
				<div class="box-body">
<!--					--><?PHP /*echo $this->renderPartial('_search',array('model'=>$model),true) */?>

					<?PHP
					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<div class="col-sm-2 pull-left page-sizes"  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
					<?php
					$this->endWidget();
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'platform-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'filter' => $model,

						'dataProvider' => $model->search(),
						'rowCssClassExpression'=>'$data->generated == 1?"active":"danger"',
 						'columns' => array(
							array(
								'name'=>'id',
								'visible'=>$model->visible_id?true:false,
							),
							array(
								'name'=>'from_date',
								'visible'=>$model->visible_from_date?true:false,

							),
							array(
								'name'=>'to_date',
								'visible'=>$model->visible_to_date?true:false,

							),							array(
								'name'=>'media_url',
								'value'=>'$data->generated==1?CHtml::link("PDF File" ,$data->media_url,array("target"=>"_blank")): "Processing "',
								'type'=>'raw',
								'visible'=>$model->visible_media_url?true:false,

							),
							array(
								'name'=>'created_at',
								'visible'=>$model->visible_created_at?true:false,

							),

							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{delete}',
								'buttons' => array(
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),
								)
							),
						)));?>
				</div>
			</div>
		</div>
</section>

<!--Tour -->
<?php
$createLink = Yii::app()->createUrl('pdf',array('#' => 'guider=third'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstpdf',
		'title'        => 'From Date',
		'next'         => 'second',
		'buttons'      => array(
			array('name'=>'Previous','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$createLink';}"),

			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Previously defined Starting date',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#platform-grid_c0',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'To Date',
		'next'         => 'third',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('firstpdf');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Previously defined end date',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#platform-grid_c1',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'PDF file',
		'next'         => 'fourth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('second');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The PDF title that was downloaded',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#platform-grid_c2',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'Created at',
		'next'         => 'fifth',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('third');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'The date that this pdf was downloaded',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#platform-grid_c3',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Option view',
		'next'         => 'six',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){ $('#yw1').addClass('showhideitem'); guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fourth'); }"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'View this item alone',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-eye',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'six',
		'title'        => 'Option edit',
		'next'         => 'seven',
		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('fifth');}"
			),
			array(
				'name'   => 'Next',

			),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'update this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-pencil-square-o',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=seventeen'));

$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'seven',
		'title'        => 'Option remove',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('six');}"
			),

			array('name'=>'Continue ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),

			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Delete this item',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '.fa-times',
		'position'      => 9,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
/* @var $this PostQueueController */
/* @var $data PostQueue */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post')); ?>:</b>
	<?php echo CHtml::encode($data->post); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('schedule_date')); ?>:</b>
	<?php echo CHtml::encode($data->schedule_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catgory_id')); ?>:</b>
	<?php echo CHtml::encode($data->catgory_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_posted')); ?>:</b>
	<?php echo CHtml::encode($data->is_posted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_id')); ?>:</b>
	<?php echo CHtml::encode($data->post_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('is_scheduled')); ?>:</b>
	<?php echo CHtml::encode($data->is_scheduled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('platform_id')); ?>:</b>
	<?php echo CHtml::encode($data->platform_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('generated')); ?>:</b>
	<?php echo CHtml::encode($data->generated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	*/ ?>

</div>
<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */

$this->pageTitle = "Post queue | Update";

$this->breadcrumbs=array(
	'Post Queues'=>array('main'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);


?>

<script>

	window.App = {};

	$(window).load(function (){


		$(function(){
			var test = localStorage.input === 'true'? true: false;
			$('#calltoaction').prop('checked', test);
		});

		$('#calltoaction').on('change', function() {
			localStorage.input = $(this).is(':checked');
			console.log($(this).is(':checked'));
		});

		if($('#calltoaction').is(":checked")) {
			$( "#PostQueue_type_call_to_action" ).prop( "disabled", false );
			$( "#PostQueue_call_to_action" ).prop( "disabled", false );

		}else{
			$( "#PostQueue_type_call_to_action" ).prop( "disabled", true );
			$( "#PostQueue_call_to_action" ).prop( "disabled", true );

		}

		if($('#PostQueue_platform_id option:selected').val() ==1){
			$('#all_call_to_action').show();

		}else{
			$('#all_call_to_action').hide();

		}


		$('#calltoaction').click(function(){
			if($('#calltoaction').is(":checked")) {
				$( "#PostQueue_type_call_to_action" ).prop( "disabled", false );
				$( "#PostQueue_call_to_action" ).prop( "disabled", false );

			}else{
				$( "#PostQueue_type_call_to_action" ).prop( "disabled", true );
				$( "#PostQueue_call_to_action" ).prop( "disabled", true );

			}
		});

		$('.tooltips').hide();

		$platform = $('#PostQueue_platform_id');
		$type = $('#PostQueue_type');
		$group = $('#type_PostQueue');
		$file_group = $('#file_PostQueue');
		$youtube_group = $('#youtube_PostQueue');
		$link_group = $('#link_PostQueue');


		if($('#hidden-type').val() != null){
			$type.val($('#hidden-type').val().toLowerCase());
		}

		App.change=function(){
			if($platform.val() != ''){
				$type.find('option').remove().end();
				$.post('<?PHP echo CController::createUrl('/PostQueue/getAll/') ?>',{id:$platform.val()}, function( data ) {
					if(data !='') {
						$group.show(); var toAppend = ''; data = jQuery.parseJSON( data);
						$.each(data,function(i,o){toAppend += '<option value="'+i+'">'+o+'</option>';});
						$type.append(toAppend);
					}
					if($type.val() == 'text'){
						$file_group.hide();
						$youtube_group.hide();
						$link_group.hide();
					}else if($type.val() == 'preview'){
						$link_group.show();
						$file_group.show();
						$youtube_group.hide();
					}else if($type.val() == 'youtube'){
						$youtube_group.show();
						$file_group.hide();
						$link_group.hide();
					}else{
						if($type.val() == 'image')
							$('#change_label').text('Upload Image');
						else
							$('#change_label').text('Upload Video');
						$file_group.show();
						$youtube_group.hide();
						$link_group.hide();
					}
				});
			}else{
				$group.hide();
			}
		};
		$('#PostQueue_platform_id').change(function(){
			if($('#PostQueue_platform_id option:selected').val() ==1){
				$('#all_call_to_action').show();

			}else{
				$('#all_call_to_action').hide();

			}
		});

		if($type.val() == 'text'){
			$file_group.hide();
			$youtube_group.hide();
			$link_group.hide();
		}else if($type.val() == 'preview'){
			$link_group.show();
			$file_group.show();
			$youtube_group.hide();
		}else if($type.val() == 'youtube'){
			$youtube_group.show();
			$file_group.hide();
			$link_group.hide();
		}else{
			if($type.val() == 'image')
				$('#change_label').text('Upload Image');
			else
				$('#change_label').text('Upload Video');
			$file_group.show();
			$youtube_group.hide();
			$link_group.hide();
		}

		$type.change(function(){
			if($type.val() == 'text'){
				$file_group.hide();
				$youtube_group.hide();
				$link_group.hide();
			}else if($type.val() == 'preview'){
				$link_group.show();
				$file_group.show();
				$youtube_group.hide();
			}else if($type.val() == 'youtube'){
				$youtube_group.show();
				$file_group.hide();
				$link_group.hide();
			}else{
				if($type.val() == 'image')
					$('#change_label').text('Upload Image');
				else
					$('#change_label').text('Upload Video');
				$file_group.show();
				$youtube_group.hide();
				$link_group.hide();
			}
		});


		if($('#PostQueue_platform_id').val() == 2 ){
			$platform = $('#PostQueue_platform_id');
			$type = $("#PostQueue_type");
			$post = $("#PostQueue_post");
			$("#twitter").show();
			var num=140;
			if($type.val()=='image' || $type.val()=='video' || $type.val()=='youtube'){
				num -=24;
			}
			if($type.val()=='preview'){
				num -=22;
			}
			num = num-$post.val().length;
			$("#twitter_counter").html(num);
		}else{
			$("#twitter").hide();
		}

		$('#PostQueue_platform_id').change(function(){
			if($(this).val() == 2){
				$('#twitter').show();

			}else{
				$('#twitter').hide();
			}
		});

		$('#PostQueue_post').keyup(function(e){


			$platform = $('#PostQueue_platform_id');

			$type = $("#PostQueue_type");
			$post = $("#PostQueue_post");
			var num=140;
			var status = false;
			$platform.each(function(i){
				if($(this).val() == 2 ){

					status = true;

				}
			});

			if(status){
				if($type.val()=='image' || $type.val()=='video' || $type.val()=='youtube'){
					num -=24;
				}
				if($type.val()=='preview'){
					num -=22;
				}
				var input =num -$(this).val().length;
				if(input >0){
					$("#twitter_counter").text(input);
					$("#yw1").show();
					$("#yw0").show();
				}else{
					$("#twitter_counter").text(input);
					$("#yw1").hide();
					$("#yw0").hide();
				}
			}
		});

		$('#PostQueue_type').on('change',function(){
			$platform = $('#PostQueue_platform_id');
			$type = $("#PostQueue_type");
			$post = $("#PostQueue_post");



			$platform.each(function(i){
				if($(this).val() == 2 ){
					$("#twitter").show();
					var num=140;
					if($type.val()=='image' || $type.val()=='video' || $type.val()=='youtube'){
						num -=24;
					}
					if($type.val()=='preview'){
						num -=22;
					}
					num = num-$post.val().length;
					$("#twitter_counter").html(num);
				}else{
					$("#twitter").hide();
				}
			});
		});

		$('#EnglishDirection').click(function(){
			if($('#PostDir').hasClass('arabic-direction')){
				$('#PostDir').removeClass('arabic-direction');
			}
		})
		$('#ArabicDirections').click(function(){
			if(!$('#PostDir').hasClass('arabic-direction')){
				$('#PostDir').addClass('arabic-direction');
			}
		});
		App.select_image = function(medias){



			$.post('<?PHP echo CController::createUrl('/postQueue/update/'.$model->id) ?>', {
				medias: medias
			}, function (data) {
				$('#changedImage').attr('src',medias);
				/*
				 alert(data);
				 */
				/*location.reload();*/
			});

		};
		$('.highlited').click(function(){
			$('.highlited').removeClass('highlites')
			$(this).addClass('highlites');
		});

		function getSelectedText(el) {
			if (typeof el.selectionStart == "number") {
				return el.value.slice(el.selectionStart, el.selectionEnd);
			} else if (typeof document.selection != "undefined") {
				var range = document.selection.createRange();
				if (range.parentElement() == el) {
					return range.text;
				}
			}
			return "";
		}

		var copyBtn = document.querySelector('#input');


		copyBtn.addEventListener('click', function () {
			var urlField = document.querySelector('#PostQueue_post');
			try {
				if(document.execCommand('copy')) {



				}
			} catch (err) {
				console.log('Oops, unable to copy');
			}
			// create a Range object
			/*var range = document.createRange();
			 // set the Node to select the "range"
			 range.selectNode(urlField);
			 // add the Range to the set of window selections
			 window.getSelection().addRange(range);*/

			// execute 'copy', can't 'cut' in this case

		}, false);


		$('#PostQueue_post').mouseup(function(){


			var srcTextarea = document.getElementById("input");
			var destTextarea = document.getElementById("PostQueue_post");
			var highlight = window.getSelection();
			var spn = '<span class="highlight">' + highlight + '</span>';
			var text = $('#PostQueue_post').val();

			if(highlight !=0) {
				if(getSelectedText(copyBtn) !="")
					$('#PostQueue_post').val(text.replace(highlight, getSelectedText(copyBtn)));
			}



		});
		$('#input').mouseup(function(){
			var highlight = window.getSelection();
			if(highlight !=0) {
				$('.tooltips').show();

				setInterval(function () {
					$('.tooltips').hide();
				}, 3000);
			}

		});
	});
</script>
<style>
	#input{
		border:0px;
	}
	#input:focus{
		border:0px;
	}
	#input:active{
		border:0px;
	}

	.highlites{
		border:1px solid black;

	}
	.highlited{
		cursor: pointer;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"><h2>Post Queue || Update </h2></div>
                    <div class="col-md-3" style="padding-top: 19px;text-align: left;">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>

					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>


</section>

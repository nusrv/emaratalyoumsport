<?php
/* @var $this PostQueueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Post Queues',
);

$this->menu=array(
	array('label'=>'Create PostQueue', 'url'=>array('create')),
	array('label'=>'Manage PostQueue', 'url'=>array('admin')),
);
?>

<h1>Post Queues</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

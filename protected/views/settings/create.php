<section class="content">

<div class="row">
       <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-md-12"><h2>New custom setting</h2></div>
                </div>
                <div class="box-body">
                    <?php
                    $all = Settings::model()->findAll('id!=1'); ?>
                    <?php  $this->renderPartial('_form_settings', array('model'=>$model,'day'=>$all)); ?>

                </div>
            </div>
        </div>
</div>
</section>
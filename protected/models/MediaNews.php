<?php

/**
 * This is the model class for table "media_news".
 *
 * The followings are the available columns in table 'media_news':
 * @property integer $id
 * @property integer $news_id
 * @property string $type
 * @property string $media
 *
 * The followings are the available model relations:
 * @property News $news
 */
class MediaNews extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('news_id, type, media', 'required'),
			array('news_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>7),
			array('media', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, news_id, type, media', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::BELONGS_TO, 'News', 'news_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'news_id' => 'News',
			'type' => 'Type',
			'media' => 'Media',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('news_id',$this->news_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('media',$this->media,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function get_related_images($model){
		$words = true;
		if(isset($model->news_id)) {
			$imagesNews = MediaNews::model()->findAll('news_id =' . $model->news_id);
			foreach ($imagesNews as $mediaImages) {
				if ($mediaImages->type == 'gallery' or $mediaImages->type=='image') {
					if($words)
						echo '<h4>Change the post image from the below related images</h4>';
					echo '<div class=\'col-sm-2\'>';
					echo CHtml::image($mediaImages->media, "image", array('max-width' => '150', 'max-height' => '150', 'style' => 'margin-left:20px;','class'=>'img-responsive img-thumbnail')) . "";
					echo '<br />
 <i class="iconss fa fa-facebook" onclick="javascript:App.select_image(' . $mediaImages->news_id . ',1,' . "'$mediaImages->media'" . ')"></i><i class="iconss fa fa-twitter" onclick="javascript:App.select_image(' . $mediaImages->news_id . ',2,' . "'$mediaImages->media'" . ')"></i><i class="iconss fa fa-instagram" onclick="javascript:App.select_image(' . $model->news_id . ',3,' . "'$mediaImages->media'" . ')"></i><i class="iconss fa fa-reply-all " onclick="javascript:App.select_image(' . $model->news_id . ',0,' . "'$mediaImages->media'" . ')"></i>\'</div>';
				}
				$words=false;
			}
		}
	}
	public function get_related_images_update($model){
		if(isset($model->news_id)) {
			$imagesNews = MediaNews::model()->findAll('news_id =' . $model->news_id);
			echo '<div class="col-sm-4">'.CHtml::image($model->media_url, "image", array( 'style' => 'margin-left:20px;max-width:200px;max-height:150px;','class'=>'img-responsive img-thumbnail','id'=>'changedImage')).'</div>';
			echo '<div class="col-sm-8">';
			foreach ($imagesNews as $mediaImages) {
				if ($mediaImages->type == 'gallery' or $mediaImages->type=='image') {

						echo '<div class=\'col-sm-6 \'>';
						echo CHtml::image($mediaImages->media, "image", array('style' => 'margin-left:20px;max-width:150px;max-height:100px', 'class' => 'img-responsive img-thumbnail highlited toos', 'onclick' => "javascript:App.select_image('$mediaImages->media')")) . "</div>";
					}
			}
			echo '</div>';

		}
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MediaNews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

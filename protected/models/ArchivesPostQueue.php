<?php

/**
 * This is the model class for table "archives_post_queue".
 *
 * The followings are the available columns in table 'archives_post_queue':
 * @property integer $id
 * @property string $post
 * @property string $type
 * @property string $link
 * @property string $youtube
 * @property string $media_url
 * @property string $schedule_date
 * @property integer $catgory_id
 * @property integer $is_posted
 * @property string $post_id
 * @property integer $news_id
 * @property integer $is_scheduled
 * @property integer $platform_id
 * @property string $generated
 * @property string $errors
 * @property integer $parent_id
 * @property integer $is_archives
 * @property string $created_at
 */
class ArchivesPostQueue extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'archives_post_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post, type, schedule_date, platform_id, created_at', 'required'),
			array('catgory_id, is_posted, news_id, is_scheduled, platform_id, parent_id, is_archives', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>14),
			array('link, youtube, media_url, post_id, errors', 'length', 'max'=>255),
			array('generated', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, post, type, link, youtube, media_url, schedule_date, catgory_id, is_posted, post_id, news_id, is_scheduled, platform_id, generated, errors, parent_id, is_archives, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'post' => 'Post',
			'type' => 'Type',
			'link' => 'Link',
			'youtube' => 'Youtube',
			'media_url' => 'Media Url',
			'schedule_date' => 'Schedule Date',
			'catgory_id' => 'Catgory',
			'is_posted' => 'Is Posted',
			'post_id' => 'Post',
			'news_id' => 'News',
			'is_scheduled' => 'Is Scheduled',
			'platform_id' => 'Platform',
			'generated' => 'Generated',
			'errors' => 'Errors',
			'parent_id' => 'Parent',
			'is_archives' => 'Is Archives',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('post',$this->post,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('youtube',$this->youtube,true);
		$criteria->compare('media_url',$this->media_url,true);
		$criteria->compare('schedule_date',$this->schedule_date,true);
		$criteria->compare('catgory_id',$this->catgory_id);
		$criteria->compare('is_posted',$this->is_posted);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('news_id',$this->news_id);
		$criteria->compare('is_scheduled',$this->is_scheduled);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('generated',$this->generated,true);
		$criteria->compare('errors',$this->errors,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('is_archives',$this->is_archives);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArchivesPostQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

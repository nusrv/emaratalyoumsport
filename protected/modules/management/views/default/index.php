<?php
/* @var $this DefaultController */
/* @var $server_cpu DefaultController */
/* @var $server_memory DefaultController */
/* @var $category Category */
/* @var $all_news News */

$this->breadcrumbs=array(
	$this->module->id,
);
$this->pageTitle = "Management";
?>
<section class="content">
	<div class="row">
	<?PHP $this->renderPartial('_info',array('this',$this,'server_memory'=>$server_memory,'server_cpu'=>$server_cpu)) ?>
		</div>
		<div class="row">
	<?PHP $this->renderPartial('_info_category_and_news',array(
		'this'=>$this,
		'server_memory'=>$server_memory,
		'server_cpu'=>$server_cpu,
		'category'=>$category,
		'all_news'=>$all_news
		)) ?>
		</div>
</section>
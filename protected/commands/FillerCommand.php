<?php
class FillerCommand extends BaseCommand{

    public function run($args){
        $this->TimeZone();
        $criteria=new CDbCriteria;
       // $criteria->limit = 1;
        $criteria->condition = "DATE_FORMAT(start_date,'%Y-%m-%d') <= '".date('Y-m-d')."' and DATE_FORMAT(end_date,'%Y-%m-%d') >= '".date('Y-m-d')."' and DATE_FORMAT(last_generated,'%Y-%m-%d') != '".date('Y-m-d')."' and catch=0 ORDER BY RAND() limit 1";
        $filler = FillerData::model()->findAll($criteria);
        if(!empty($filler)){
            foreach ($filler as $item) {
                $item->catch = 1;
                $item->last_generated = date('Y-m-d');
                $item->command = false;
                $item->save();
                $PostQueue = new PostQueue();
                $PostQueue->setIsNewRecord(true);
                $PostQueue->command= false;
                $PostQueue->id= null;
                $PostQueue->type = $item->type;
                $PostQueue->post = $item->text;
                $PostQueue->schedule_date = date('Y-m-d '.$item->publish_time);
                $PostQueue->catgory_id = null;
                $PostQueue->media_url = $item->media_url;
                $PostQueue->link = $item->link;
                $PostQueue->is_posted = 0;
                $PostQueue->news_id =null;
                $PostQueue->post_id =null;
                $PostQueue->is_scheduled =1;
                $PostQueue->platform_id =$item->platform_id;
                $PostQueue->generated ='manual';
                $PostQueue->created_at =date('Y-m-d H:i:s');
                if(!$PostQueue->save())
                    $this->send_email($PostQueue,'error on filler data');
                }
        }else{
            $this->reset();
        }
    }

    private function reset(){
        $item = FillerData::model()->findAll('catch=1');
        foreach ($item as $value) {
            $value->catch = 0;
            $value->command = false;
            if(!$value->save())
                $this->send_email($value,'error on filler data reset');
        }
    }

}
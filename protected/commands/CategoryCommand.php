<?php
class CategoryCommand extends BaseCommand{

    public function run($args){
        $this->TimeZone();
        $findme   = 'رياضة';
        $html = new SimpleHTMLDOM();
        $html_data = $html->file_get_html(Yii::app()->params['feedUrl']);
        $data = $html_data->find('nav[class=mainnav] div[class=container_12] ul[class=dropdown-menu]  ', 0)->children();
        foreach ($data as $item) {
            $pos = strpos(' '.$item->find('a', 0)->plaintext, $findme);
            if ($pos) {
                $url = $item->find('a', 0)->href;
                $title = $item->find('a', 0)->plaintext;
                if (!preg_match("~^(?:f|ht)tps?://~i", $url))
                    $url = Yii::app()->params['feedUrl'] . $url;
                $cat = Category::model()->find('url = "' . $url . '"');
                if (empty($cat)) {
                    $cat = new Category();
                    $cat->url = $url;
                    $cat->title = $title;
                    $cat->active = 1;
                    $cat->command = false;
                    $cat->created_at = date('Y-m-d H:i:s');
                    $cat->deleted = 0;
                    $cat->setIsNewRecord(true);
                    if ($cat->save()) {
                        if (is_array($item->find('ul'))) {
                            foreach ($item->find('ul li a') as $item2) {
                                $url = Yii::app()->params['feedUrl'] . $item2->href;
                                $title = $item2->innertext;
                                $sub = SubCategories::model()->find('url = "' . $url . '" and category_id = ' . $cat->id);
                                if (empty($sub)) {
                                    $sub = new SubCategories();
                                    $sub->url = $url;
                                    $sub->command = false;
                                    $sub->title = $title;
                                    $sub->category_id = $cat->id;
                                    $sub->created_at = date('Y-m-d H:i:s');
                                    $sub->deleted = 0;
                                    $sub->setIsNewRecord(true);
                                    if (!$sub->save()) {
                                        $this->send_email($sub, 'error on getting sub sections');
                                    }
                                }
                            }
                        }
                    } else {
                        $this->send_email($cat, 'error on getting sections');
                    }
                }
            }
        }
    }
}